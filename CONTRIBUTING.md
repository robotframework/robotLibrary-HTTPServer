# Contributing

You are welcome to participate and improve this library. Please discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## Guide

Fork and clone the repository
Set up your environment
Make sure the current tests pass:
	
	mvn clean verify

Make your changes. Add tests (Unit and Robot) for your changes. Make the tests pass:

    mvn clean verify

Push to your fork and submit a pull request.

Your pull requests is commented and owner/authors may suggest some changes, improvements or alternatives.

Some things that will increase the chance that your pull request is accepted:

* Write tests (Junit and Robot)
  * Write Robot tests if creating new or updating existing keywords
* Follow clean coding principles
* Inspect your code quality with tool like sonarQube or SonarLint plug-in
* Write a good commit messages.
