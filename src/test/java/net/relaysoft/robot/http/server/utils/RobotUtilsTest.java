package net.relaysoft.robot.http.server.utils;

import static org.junit.Assert.*;


import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RobotUtilsTest {
	
	private static final String DICTIONARY_STRING = "{u'test1': u'test1', u'test2': u'test2'}";
	@SuppressWarnings("serial")
	static final Map<String , String> EXPECTED_MAP = new HashMap<String , String>() {{
	    put("test1", "test1");
	    put("test2", "test2");
	}};

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsDictionary() {
		assertTrue(RobotUtils.isDictionary(DICTIONARY_STRING));
	}
	
	@Test
	public void testParseDictionary() {
		assertEquals(EXPECTED_MAP, RobotUtils.parseDictionary(DICTIONARY_STRING));
	}

}
