package net.relaysoft.robot.http.server.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StringUtilsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsBlank() {
		assertFalse(StringUtils.isBlank("test"));
		assertTrue(StringUtils.isBlank(null));
		assertTrue(StringUtils.isBlank(""));
		assertTrue(StringUtils.isBlank("  "));

	}

	@Test
	public void testIsNotBlank() {
		assertTrue(StringUtils.isNotBlank("test"));
		assertFalse(StringUtils.isNotBlank(null));
		assertFalse(StringUtils.isNotBlank(""));
		assertFalse(StringUtils.isNotBlank("  "));
	}

	@Test
	public void testSubstringAfter() {
		assertEquals("UTF8", StringUtils.substringAfter("charset=UTF8", "charset="));
	}

}
