package net.relaysoft.robot.http.server.keywords;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.relaysoft.robot.http.server.MockServer;
import net.relaysoft.robot.http.server.MockServerFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MockServerFactory.class)
public class HTTPServerKeywordsTest {
	
	private static final String CONTENT = "test";
	private static final String CONTENT_TYPE = "application/json";
	private static final String FILE_PATH = "path/to/file";
	private static final String HEADERS = "{u'header1': u'value1', u'header2': u'value2'}";
	private static final String METHOD = "POST";
	private static final String NONE = "None";
	private static final String PATH = "/test";
	private static final String QUERY = "{u'test1': u'value1', u'test2': u'value2'}";
	private static final String SOAP_ACTION = "action";
	private static final String STATUS = "201";
	
	private static final int EXPECTED_STATUS = 201;
	
	@SuppressWarnings("serial")
	static final Map<String , String> EXPECTED_HEADER_MAP = new HashMap<String , String>() {{
	    put("header1", "value1");
	    put("header2", "value2");
	}};
	@SuppressWarnings("serial")
	static final Map<String , String> EXPECTED_QUERY_MAP = new HashMap<String , String>() {{
	    put("test1", "value1");
	    put("test2", "value2");
	}};
	
	
	MockServer mockServer;
	
	HTTPServerKeywords keywords;
	
	Map<String, String> headers;
	Map<String, String> queryParams;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		mockServer = mock(MockServer.class);
		queryParams = mock(Map.class);
		headers = mock(Map.class);
		when(mockServer.getRequestHeaders()).thenReturn(headers);
		when(mockServer.getRequestHeaders(anyInt())).thenReturn(headers);
		when(mockServer.getPort()).thenReturn(8888);
		PowerMockito.mockStatic(MockServerFactory.class);
		when(MockServerFactory.getMockServer()).thenReturn(mockServer);
		keywords = new HTTPServerKeywords();
		keywords.initializeHTTPServer();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetHTTPRequestContent() {
		keywords.getHTTPRequestContent();
		verify(mockServer).getRequestBody();
		keywords.getHTTPRequestContent(1);
		verify(mockServer).getRequestBody(1);
	}

	@Test
	public void testGetHTTPRequestHeader() {
		keywords.getHTTPRequestHeader("test1");
		verify(mockServer).getRequestHeaders();
		verify(headers).get("test1");
		keywords.getHTTPRequestHeader("test2", 1);
		verify(mockServer).getRequestHeaders(1);
		verify(headers).get("test2");
	}

	@Test
	public void testGetHTTPRequestHeaders() {
		keywords.getHTTPRequestHeaders();
		verify(mockServer).getRequestHeaders();
		keywords.getHTTPRequestHeaders(1);
		verify(mockServer).getRequestHeaders(1);
	}

	@Test
	public void testGetHTTPRequestMethod() {
		keywords.getHTTPRequestMethod();
		verify(mockServer).getRequestMethod();
		keywords.getHTTPRequestMethod(1);
		verify(mockServer).getRequestMethod(1);
	}

	@Test
	public void testGetHTTPRequestQueryString() {
		keywords.getHTTPRequestQueryString();
		verify(mockServer).getRequestQueryString();
		keywords.getHTTPRequestQueryString(1);
		verify(mockServer).getRequestQueryString(1);
	}

	@Test
	public void testGetHTTPServerPort() {
		keywords.getHTTPServerPort();
		verify(mockServer).getPort();
	}
	
	@Test
	public void testInitializeHTTPServer() throws IOException {
		keywords.initializeHTTPServer(8888);
		verify(mockServer).initializeServer(8888);
	}

	@Test
	public void testResetHTTPRequestContents() {
		keywords.resetHTTPRequestContents();
		verify(mockServer).resetRequestData();
	}

	@Test
	public void testResetHTTPResponseContents() {
		keywords.resetHTTPResponseContents();
		verify(mockServer).resetResponseData();
	}
	
	@Test
	public void testSetDefaultResponse() {
		keywords.setDefaultResponse(new String[] {NONE, NONE, NONE, NONE});
		verify(mockServer).setDefaultResponse(null, null, 200, new HashMap<>());	
		keywords.setDefaultResponse(new String[] {CONTENT, NONE, NONE, NONE});
		verify(mockServer).setDefaultResponse(CONTENT, null, 200, new HashMap<>());
		keywords.setDefaultResponse(new String[] {CONTENT, STATUS, NONE, NONE});
		verify(mockServer).setDefaultResponse(CONTENT, null, EXPECTED_STATUS, new HashMap<>());
		keywords.setDefaultResponse(new String[] {CONTENT, STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setDefaultResponse(CONTENT, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());		
		keywords.setDefaultResponse(new String[] {CONTENT, STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setDefaultResponse(CONTENT, "application/json", EXPECTED_STATUS, EXPECTED_HEADER_MAP);
	}

	@Test
	public void testSetDefaultResponseFromFile() {
		keywords.setDefaultResponseFromFile(FILE_PATH, new String[] {NONE, NONE, NONE});
		verify(mockServer).setDefaultResponseFromFile(FILE_PATH, null, 200, new HashMap<>());			
		keywords.setDefaultResponseFromFile(FILE_PATH, new String[] {STATUS, NONE, NONE});
		verify(mockServer).setDefaultResponseFromFile(FILE_PATH, null, EXPECTED_STATUS, new HashMap<>());			
		keywords.setDefaultResponseFromFile(FILE_PATH, new String[] {STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setDefaultResponseFromFile(FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());			
		keywords.setDefaultResponseFromFile(FILE_PATH, new String[] {STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setDefaultResponseFromFile(FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, EXPECTED_HEADER_MAP);		
	}

	@Test
	public void testSetRESTResponse() {
		keywords.setRESTResponse(PATH, METHOD, new String[] {NONE, NONE, NONE, NONE, NONE});
		verify(mockServer).setRestResponseData(PATH, METHOD, new HashMap<>(), null, null, 200, new HashMap<>());
		keywords.setRESTResponse(PATH, METHOD, new String[] {QUERY, NONE, NONE, NONE, NONE});
		verify(mockServer).setRestResponseData(PATH, METHOD, EXPECTED_QUERY_MAP, null, null, 200, new HashMap<>());
		keywords.setRESTResponse(PATH, METHOD, new String[] {QUERY, CONTENT, NONE, NONE, NONE});
		verify(mockServer).setRestResponseData(PATH, METHOD, EXPECTED_QUERY_MAP, CONTENT, null, 200, new HashMap<>());
		keywords.setRESTResponse(PATH, METHOD, new String[] {QUERY, CONTENT, STATUS, NONE, NONE});
		verify(mockServer).setRestResponseData(PATH, METHOD, EXPECTED_QUERY_MAP, CONTENT, null, EXPECTED_STATUS, new HashMap<>());
		keywords.setRESTResponse(PATH, METHOD, new String[] {QUERY, CONTENT, STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setRestResponseData(PATH, METHOD, EXPECTED_QUERY_MAP, CONTENT, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());
		keywords.setRESTResponse(PATH, METHOD, new String[] {QUERY, CONTENT, STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setRestResponseData(PATH, METHOD, EXPECTED_QUERY_MAP, CONTENT, CONTENT_TYPE, EXPECTED_STATUS, EXPECTED_HEADER_MAP);
	}

	@Test
	public void testSetRESTResponseFromFile() {
		keywords.setRESTResponseFromFile(PATH, METHOD, FILE_PATH, new String[] {NONE, NONE, NONE, NONE});
		verify(mockServer).setRestResponseDataFromFile(PATH, METHOD, new HashMap<>(), FILE_PATH, null, 200, new HashMap<>());
		keywords.setRESTResponseFromFile(PATH, METHOD, FILE_PATH, new String[] {QUERY, NONE, NONE, NONE});
		verify(mockServer).setRestResponseDataFromFile(PATH, METHOD, EXPECTED_QUERY_MAP, FILE_PATH, null, 200, new HashMap<>());
		keywords.setRESTResponseFromFile(PATH, METHOD, FILE_PATH, new String[] {QUERY, STATUS, NONE, NONE});
		verify(mockServer).setRestResponseDataFromFile(PATH, METHOD, EXPECTED_QUERY_MAP, FILE_PATH, null, EXPECTED_STATUS, new HashMap<>());
		keywords.setRESTResponseFromFile(PATH, METHOD, FILE_PATH, new String[] {QUERY, STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setRestResponseDataFromFile(PATH, METHOD, EXPECTED_QUERY_MAP, FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());
		keywords.setRESTResponseFromFile(PATH, METHOD, FILE_PATH, new String[] {QUERY, STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setRestResponseDataFromFile(PATH, METHOD, EXPECTED_QUERY_MAP, FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, EXPECTED_HEADER_MAP);
	}

	@Test
	public void testSetSOAPResponse() {
		keywords.setSOAPResponse(PATH, SOAP_ACTION, CONTENT, new String[] {NONE, NONE, NONE});
		verify(mockServer).setSoapResponseData(PATH, SOAP_ACTION, CONTENT, "application/soap+xml", 200, new HashMap<>());
		keywords.setSOAPResponse(PATH, SOAP_ACTION, CONTENT, new String[] {STATUS, NONE, NONE});
		verify(mockServer).setSoapResponseData(PATH, SOAP_ACTION, CONTENT, "application/soap+xml", EXPECTED_STATUS, new HashMap<>());
		keywords.setSOAPResponse(PATH, SOAP_ACTION, CONTENT, new String[] {STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setSoapResponseData(PATH, SOAP_ACTION, CONTENT, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());
		keywords.setSOAPResponse(PATH, SOAP_ACTION, CONTENT, new String[] {STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setSoapResponseData(PATH, SOAP_ACTION, CONTENT, CONTENT_TYPE, EXPECTED_STATUS, EXPECTED_HEADER_MAP);
	}

	@Test
	public void testSetSOAPResponseFromFile() {
		keywords.setSOAPResponseFromFile(PATH, SOAP_ACTION, FILE_PATH, new String[] {NONE, NONE, NONE});
		verify(mockServer).setSoapResponseDataFromFile(PATH, SOAP_ACTION, FILE_PATH, "application/soap+xml", 200, new HashMap<>());
		keywords.setSOAPResponseFromFile(PATH, SOAP_ACTION, FILE_PATH, new String[] {STATUS, NONE, NONE});
		verify(mockServer).setSoapResponseDataFromFile(PATH, SOAP_ACTION, FILE_PATH, "application/soap+xml", EXPECTED_STATUS, new HashMap<>());
		keywords.setSOAPResponseFromFile(PATH, SOAP_ACTION, FILE_PATH, new String[] {STATUS, CONTENT_TYPE, NONE});
		verify(mockServer).setSoapResponseDataFromFile(PATH, SOAP_ACTION, FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, new HashMap<>());
		keywords.setSOAPResponseFromFile(PATH, SOAP_ACTION, FILE_PATH, new String[] {STATUS, CONTENT_TYPE, HEADERS});
		verify(mockServer).setSoapResponseDataFromFile(PATH, SOAP_ACTION, FILE_PATH, CONTENT_TYPE, EXPECTED_STATUS, EXPECTED_HEADER_MAP);
	}

	@Test
	public void testStartHTTPServer() throws Exception {
		keywords.startHTTPServer();
		verify(mockServer).start();
	}

	@Test
	public void testStopHTTPServer() throws Exception {
		keywords.stopHTTPServer();
		verify(mockServer).stop();
	}

}
