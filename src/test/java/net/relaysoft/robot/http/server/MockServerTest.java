package net.relaysoft.robot.http.server;

import static org.junit.Assert.*;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MockServerTest {

	private static final String TEST_FILE_BASE_PATH = System.getProperty("user.dir") + "/src/test/resources/file/";

	private static final String TEST_REQUEST_CONTENT1 = "Hello World";
	private static final String TEST_REQUEST_CONTENT_TYPE1 = "text/html";
	private static final String TEST_REQUEST_CONTENT_TYPE2 = "text/plain";
	private static final String TEST_REQUEST_PATH1 = "test1";
	private static final String TEST_REQUEST_PATH2 = "test2";
	private static final String TEST_REQUEST_METHOD1 = "POST";
	private static final String TEST_REQUEST_METHOD2 = "GET";
	private static final String TEST_REQUEST_QUERYSTRING2 = "test1=value1&test2=value2";

	private static final String TEST_RESPONSE_CONTENT_FILE1 = "test1.txt";
	private static final String TEST_RESPONSE_CONTENT_FILE2 = "test2.txt";
	private static final String TEST_RESPONSE_HEADER_NAME1 = "test1";
	private static final String TEST_RESPONSE_HEADER_NAME2 = "test2";
	private static final String TEST_RESPONSE_HEADER_VALUE1 = "value1";
	private static final String TEST_RESPONSE_HEADER_VALUE2 = "value2";

	private static final String AUTH_TYPE = "Basic";
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	
	MockServer server;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		server = new MockServer();
		server.initializeServer(0);
		server.start();
	}

	@After
	public void tearDown() throws Exception {
		server.resetResponseData();
		server.resetRequestData();
		server.stop();
	}

	@Test
	public void testInitializeServer() throws IOException {
		server.initializeServer(8888);
		assertEquals(8888, server.getPort());
		server.initializeServer(0);
		assertTrue(server.getPort() > 0);
	}

	@Test
	public void testStart() throws Exception {
		server.start();
	}

	@Test
	public void testStop() throws Exception {
		server.stop();
	}

	@Test
	public void testGetPort() {
		assertTrue(server.getPort() > 0);
	}

	@Test
	public void testGetRequestAuthType() throws IOException {
		executeTestRequests();		
		assertNull(server.getRequestAuthType());
		assertEquals(AUTH_TYPE, server.getRequestAuthType(1));
		assertNull(server.getRequestAuthType(2));		
	}

	@Test
	public void testGetRequestBody() throws IOException {
		executeTestRequests();
		assertEquals("", server.getRequestBody());
		assertEquals(TEST_REQUEST_CONTENT1, server.getRequestBody(1));
		assertEquals("", server.getRequestBody(2));	
	}

	@Test
	public void testGetRequestHeaders() throws IOException {
		executeTestRequests();
		Map<String, String> headers = server.getRequestHeaders();
		assertEquals(5, headers.size());
		assertEquals(TEST_REQUEST_CONTENT_TYPE2, headers.get("Content-Type"));
		headers = server.getRequestHeaders(1);
		assertEquals(7, headers.size());
		assertEquals(TEST_REQUEST_CONTENT_TYPE1, headers.get("Content-Type"));
		headers = server.getRequestHeaders(2);
		assertEquals(5, headers.size());
		assertEquals(TEST_REQUEST_CONTENT_TYPE2, headers.get("Content-Type"));	
	}

	@Test
	public void testGetRequestMethod() throws IOException {
		executeTestRequests();
		assertEquals(TEST_REQUEST_METHOD2, server.getRequestMethod());
		assertEquals(TEST_REQUEST_METHOD1, server.getRequestMethod(1));
		assertEquals(TEST_REQUEST_METHOD2, server.getRequestMethod(2));	
	}

	@Test
	public void testGetRequestQueryString() throws IOException {
		executeTestRequests();		
		assertEquals(TEST_REQUEST_QUERYSTRING2, server.getRequestQueryString());
		assertNull(server.getRequestQueryString(1));
		assertEquals(TEST_REQUEST_QUERYSTRING2, server.getRequestQueryString(2));
	}

	@Test
	public void testResetRequestData() throws IOException {
		executeTestRequests();
		assertEquals(2, server.getRequestCount());
		server.resetRequestData();
		assertEquals(0, server.getRequestCount());
	}

	@Test
	public void testResetResponseData() throws IOException {
		server.setDefaultResponse("test", "text/html", 201, createTestResponseHeaders());
		assertRequest(createRequestUrl("/test", null), "POST", null, "test", "text/html", 201, createTestResponseHeaders());
		server.resetResponseData();
		assertRequest(createRequestUrl("/test", null), "POST", null, null, "text/plain", 404, null);
	}

	@Test
	@Parameters(method = "defaultResponseValues")
	public void testSetDefaultResponse(String content, String contentType, int statusCode, Map<String, String> headers) throws IOException {
		server.setDefaultResponse(content, contentType, statusCode, headers);
		assertRequest(createRequestUrl("/", null), "POST", null, content, contentType, statusCode, headers);
		assertRequest(createRequestUrl("/test1", null), "GET", null, content, contentType, statusCode, headers);
		assertRequest(createRequestUrl("/test2", createTestQueryParams()), "DELETE", null, content, contentType, statusCode, headers);
	}

	@Test
	@Parameters(method = "defaultResponseFromFileValues")
	public void testSetDefaultResponseFromFile(String filePath, String contentType, int statusCode, Map<String, String> headers) 
			throws IOException {
		server.setDefaultResponseFromFile(filePath, contentType, statusCode, headers);
		String expectedContent = IOUtils.toString(new FileInputStream(filePath), StandardCharsets.UTF_8);
		assertRequest(createRequestUrl("/", null), "POST", null, expectedContent, contentType, statusCode, headers);
		assertRequest(createRequestUrl("/test1", null), "GET", null, expectedContent, contentType, statusCode, headers);
		assertRequest(createRequestUrl("/test2", createTestQueryParams()), "DELETE", null, expectedContent, contentType, statusCode, headers);
	}

	@Test
	@Parameters(method = "restResponseValues")
	public void testSetRestResponseData(String path, String method, Map<String, String> queryParam, String content, String contentType, int statusCode, 
			Map<String, String> headers) throws IOException {
		server.setRestResponseData(path, method, queryParam, content, contentType, statusCode, headers);
		assertRequest(createRequestUrl(path, queryParam), method, null, content, contentType, statusCode, headers);
	}	

	@Test
	@Parameters(method = "restResponseFromFileValues")
	public void testSetRestResponseDataFromFile(String path, String method, Map<String, String> queryParam, String filePath, String contentType, int statusCode, 
			Map<String, String> headers) throws IOException {
		server.setRestResponseDataFromFile(path, method, queryParam, filePath, contentType, statusCode, headers);
		String expectedContent = IOUtils.toString(new FileInputStream(filePath), StandardCharsets.UTF_8);
		assertRequest(createRequestUrl(path, queryParam), method, null, expectedContent, contentType, statusCode, headers);
	}

	@Test
	@Parameters(method = "soapResponseValues")
	public void testSetSoapResponseData(String path, String soapAction, String body, String contentType, int statusCode, 
			Map<String, String> headers) throws IOException {
		server.setSoapResponseData(path, soapAction, body, contentType, statusCode, headers);
		assertRequest(createRequestUrl(path, null), "POST", soapAction, body, contentType, statusCode, headers);
	}

	@Test
	@Parameters(method = "soapResponseFromFileValues")
	public void testSetSoapResponseDataFromFile(String path, String soapAction, String filePath, String contentType, int statusCode, 
			Map<String, String> headers) throws IOException {
		server.setSoapResponseDataFromFile(path, soapAction, filePath, contentType, statusCode, headers);
		String expectedContent = IOUtils.toString(new FileInputStream(filePath), StandardCharsets.UTF_8);
		assertRequest(createRequestUrl(path, null), "POST", soapAction, expectedContent, contentType, statusCode, headers);
	}
	
	Object[] defaultResponseValues() {
		return new Object[]{
				new Object[]{"content1", "text/html", 200, createTestResponseHeaders()},
				new Object[]{"content2", "text/plain", 201, createTestResponseHeaders()}
		};
	}
	
	Object[] defaultResponseFromFileValues() {
		return new Object[]{
				new Object[]{TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE1, "text/html", 200, createTestResponseHeaders()},
				new Object[]{TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE2, "text/plain", 201, createTestResponseHeaders()}
		};
	}
	
	Object[] restResponseValues() {
		return new Object[]{
				new Object[]{"test1", "POST", null, "content1", "text/html", 200, createTestResponseHeaders()},
				new Object[]{"test2", "GET", createTestQueryParams(), "content2", "text/plain", 201, createTestResponseHeaders()}
		};
	}
	
	Object[] restResponseFromFileValues() {
		return new Object[]{
				new Object[]{"test1", "POST", null, TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE1, "text/html", 200, 
						createTestResponseHeaders()},
				new Object[]{"test2", "GET", createTestQueryParams(), TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE2, "text/plain", 201, 
						createTestResponseHeaders()}
		};
	}
	
	Object[] soapResponseValues() {
		return new Object[]{
				new Object[]{"test1", "testAction1", "content1", "text/html", 200, createTestResponseHeaders()},
				new Object[]{"test1", "testAction2", "content2", "text/plain", 201, createTestResponseHeaders()}
		};
	}
	
	Object[] soapResponseFromFileValues() {
		return new Object[]{
				new Object[]{"test1", "testAction1", TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE1, "text/html", 200, 
						createTestResponseHeaders()},
				new Object[]{"test1", "testAction2", TEST_FILE_BASE_PATH + TEST_RESPONSE_CONTENT_FILE2, "text/plain", 201, 
						createTestResponseHeaders()}
		};
	}

	private void assertRequest(String urlString, String requestMethod, String soapAction, String expectedBody, String expectedContentType, 
			int expectedStatusCode, Map<String, String> expectedHeaders) throws IOException {
		HttpURLConnection con = connect(urlString, requestMethod, soapAction);
		assertEquals(expectedContentType, con.getContentType());
		assertEquals(expectedStatusCode, con.getResponseCode());
		if(expectedBody != null){
			String body = null;
			if(expectedStatusCode > 400){
				body = IOUtils.toString(con.getErrorStream(), StandardCharsets.UTF_8);
			} else {
				body = IOUtils.toString(con.getInputStream(), StandardCharsets.UTF_8);
			}
			assertEquals(expectedBody, body);
		}
		if(expectedHeaders != null){
			for(Entry<String, String> entry : expectedHeaders.entrySet()){
				assertEquals(entry.getValue(), con.getHeaderField(entry.getKey()));
			}
		}
		con.disconnect();
	}

	private HttpURLConnection connect(String urlString, String requestMethod, String soapAction) throws IOException{
		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(requestMethod);
		con.setRequestProperty("Accept", "text/plain");
		if(soapAction != null){
			con.setRequestProperty("SOAPAction", soapAction);
		}
		con.connect();
		return con;
	}

	private String createAuthenticationString(){
		String userPassword = USERNAME + ":" + PASSWORD;
		return Base64.getEncoder().encodeToString(userPassword.getBytes());
	}
	
	private String createRequestUrl(String path, Map<String, String> queryParams){
		String url = getBaseUrl();
		url = path != null ? url + path : url; 
		String queryString = convertMapToString(queryParams);
		url = queryString != null ? url + queryString : url;
		return url;
	}

	private Map<String, String> createTestResponseHeaders(){
		Map<String, String> headers = new HashMap<>();
		headers.put(TEST_RESPONSE_HEADER_NAME1, TEST_RESPONSE_HEADER_VALUE1);
		headers.put(TEST_RESPONSE_HEADER_NAME2, TEST_RESPONSE_HEADER_VALUE2);
		return headers;
	}
	
	private Map<String, String> createTestQueryParams(){
		Map<String, String> headers = new HashMap<>();
		headers.put("test1", "value1");
		headers.put("test2", "value2");
		return headers;
	}

	private void executeTestRequests() throws IOException{
		server.setDefaultResponse(null, null, 200, null);
		executeTestRequest1();
		executeTestRequest2();
	}

	private void executeTestRequest1() throws IOException{
		URL url = new URL(getBaseUrl() + TEST_REQUEST_PATH1);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(TEST_REQUEST_METHOD1);
		con.addRequestProperty("Authorization", AUTH_TYPE + " " + createAuthenticationString());
		con.addRequestProperty("Content-Type", TEST_REQUEST_CONTENT_TYPE1);
		con.setDoOutput(true);
		con.connect();
		DataOutputStream out = new DataOutputStream(con.getOutputStream());
		out.writeBytes(TEST_REQUEST_CONTENT1);
		out.flush();
		out.close();
		assertEquals(200, con.getResponseCode());
		con.disconnect();
	}

	private void executeTestRequest2() throws IOException{
		URL url = new URL(getBaseUrl() + TEST_REQUEST_PATH2 + "?" + TEST_REQUEST_QUERYSTRING2);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(TEST_REQUEST_METHOD2);
		con.addRequestProperty("Content-Type", TEST_REQUEST_CONTENT_TYPE2);
		con.connect();
		assertEquals(200, con.getResponseCode());
		con.disconnect();
	}

	private String getBaseUrl(){
		return "http://localhost:" + server.getPort() + "/";
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	private String convertMapToString(Map<String, String> map){
		String result = null;
		if(map != null && !map.isEmpty()){
			result = map.entrySet()
					.stream()
					.map(entry -> entry.getKey() + "=" + entry.getValue())
					.collect(Collectors.joining("&"));
			result = "?".concat(result);
		}
		return result;
	}

}
