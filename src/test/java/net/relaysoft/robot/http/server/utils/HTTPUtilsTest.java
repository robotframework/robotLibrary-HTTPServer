package net.relaysoft.robot.http.server.utils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class HTTPUtilsTest {
	
	private static final Map<String, String> EXPECTED_QUERY_STRING_MAP;
	
	static {
		EXPECTED_QUERY_STRING_MAP = new HashMap<String, String>();
		EXPECTED_QUERY_STRING_MAP.put("param1", "value1");
		EXPECTED_QUERY_STRING_MAP.put("param2", "value2");
    }
	
	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Rule 
	public MockitoRule mockitoRule = MockitoJUnit.rule(); 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertQueryStringIntoMap() {
		Map<String, String> result = HTTPUtils.convertQueryStringIntoMap("param1=value1&param2=value2");
		assertThat(result, is(EXPECTED_QUERY_STRING_MAP));
	}
	
	@Test
	public void testConvertQueryStringIntoMap_empty() {
		Map<String, String> result = HTTPUtils.convertQueryStringIntoMap("");
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAuthType() {
		when(httpServletRequest.getAuthType()).thenReturn("BASIC");
		String result = HTTPUtils.getAuthType(httpServletRequest);
		assertEquals("BASIC", result);
	}
	
	@Test
	public void testGetAuthType_fromHeader() {
		when(httpServletRequest.getAuthType()).thenReturn(null);
		when(httpServletRequest.getHeader(HTTPUtils.HEADER_AUTHORIZATION)).thenReturn("BASIC xxx");
		String result = HTTPUtils.getAuthType(httpServletRequest);
		assertEquals("BASIC", result);
	}
	
	@Test
	public void testGetAuthType_empty() {
		when(httpServletRequest.getAuthType()).thenReturn(null);
		when(httpServletRequest.getHeader(HTTPUtils.HEADER_AUTHORIZATION)).thenReturn(null);
		when(httpServletRequest.getAuthType()).thenReturn(null);
		String result = HTTPUtils.getAuthType(httpServletRequest);
		assertNull(result);
	}

	@Test
	public void testGetCharsetFromContentType() {
		String result = HTTPUtils.getCharsetFromContentType("plain/text charset=UTF-16");
		assertEquals("UTF-16", result);
	}
	
	@Test
	public void testGetCharsetFromContentType_empty() {
		String result = HTTPUtils.getCharsetFromContentType("plain/text");
		assertEquals("UTF-8", result);
	}

}
