*** Settings ***
library         Collections
library         String
library         OperatingSystem
library         HttpRequestLibrary
library         HTTPServer

*** Variables ***
${DEFAULT_CONTENT}                default content
${DEFAULT_CONTENT_TYPE}           text/xml
&{DEFAULT_HEADERS}                header1=value1  header2=value2
${DEFAULT_STATUS}                 ${200}
${REST_CONTENT_GET_OK}            REST OK
${REST_CONTENT_TYPE_GET_OK}       text/xml
${REST_PATH_GET_OK}               /test/get
${REST_STATUS_GET_OK}             ${200}
${REST_CONTENT_GET_NOT_OK}        REST NOT OK
${REST_CONTENT_TYPE_GET_NOT_OK}   text/xml
&{REST_QUERY_PARAMS_GET_NOT_OK}   test1=test1  test2=test2
${REST_QUERY_GET_NOT_OK}          test=test
${REST_PATH_GET_NOT_OK}           /test/get
${REST_STATUS_GET_NOT_OK}         ${500}


*** Keywords ***
Setup Responses
    Set Default Response  content=${DEFAULT_CONTENT}  statusCode=${DEFAULT_STATUS}  contentType=${DEFAULT_CONTENT_TYPE}  headers=&{DEFAULT_HEADERS}
    
Setup Test Case
    Reset Mock Server
    Setup Responses

Setup Test Suite
    Initialize HTTP Server  ${0}
    Start HTTP Server
    ${port} =  Get HTTP Server Port 
    Create Session  client  http://localhost:${port}

Reset Mock Server
    Reset HTTP Request Contents
    Reset HTTP Response Contents

Teardown Test Suite
    Stop HTTP Server