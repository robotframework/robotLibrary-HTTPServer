*** Settings ***
resource        ../resources/common.robot
Suite Setup     Setup Test Suite
Suite Teardown  Teardown Test Suite

*** Variables ***
${CONTENT}                test content
${CONTENT_TYPE}           text/plain
${FILE_PATH}              ${CURDIR}/../resources/content.txt
${FILE_CONTENT}           file content 
&{HEADERS}                header1=value1  header2=value2

*** Test Cases ***
Test Default Response With Get
    [Documentation]  Test default response with GET method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response  content=${CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test?param=value
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.queryString}  param=value
    
Test Default Response From File With Get
    [Documentation]  Test default response from file with GET method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response From File  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test?param=value
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.queryString}  param=value
    
Test Default Response With Post
    [Documentation]  Test default response with POST method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response  content=${CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Post Request  client  uri=/test  data=request content
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.body}  request content
    
Test Default Response From File With Post
    [Documentation]  Test default response from file with POST method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response From File  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Post Request  client  uri=/test  data=request content
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.body}  request content
    
Test Default Response With Put
    [Documentation]  Test default response with PUT method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response  content=${CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Put Request  client  uri=/test/id  data=request content
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  PUT
    Should Be Equal  ${request.path}  /test/id
    Should Be Equal  ${request.body}  request content
    
Test Default Response From File With Put
    [Documentation]  Test default response from file with PUT method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response From File  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Put Request  client  uri=/test/id  data=request content
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  PUT
    Should Be Equal  ${request.path}  /test/id
    Should Be Equal  ${request.body}  request content
    
Test Default Response With Delete
    [Documentation]  Test default response with DELETE method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response  content=${CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Delete Request  client  /test/id
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  DELETE
    Should Be Equal  ${request.path}  /test/id
    
Test Default Response FRom File With Delete
    [Documentation]  Test default response with DELETE method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set Default Response From File  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Delete Request  client  /test/id
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  DELETE
    Should Be Equal  ${request.path}  /test/id