*** Settings ***
resource        ../resources/common.robot
Suite Setup     Setup Test Suite
Suite Teardown  Teardown Test Suite

*** Variables ***
${RESPONSE_CONTENT}       response content
${REQUEST_CONTENT}        request content
${CONTENT_TYPE}           text/plain
${FILE_PATH}              ${CURDIR}/../resources/content.txt
${FILE_CONTENT}           file content 
&{HEADERS}                header1=value1  header2=value2
&{QUERY_PARAMS}           param1=value1  param2=value2

*** Test Cases ***
Test REST Response With Get
    [Documentation]  Test REST response with GET method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response  path=test  method=GET  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    
Test REST Response From File With Get
    [Documentation]  Test REST response from file with GET method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response From File  path=test  method=GET  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    
Test REST Response With Get And Query String
    [Documentation]  Test REST response with GET method and query string
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response  path=test  method=GET  queryParameters=&{QUERY_PARAMS}  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test?param1=value1&param2=value2
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.queryString}  param1=value1&param2=value2
    
Test REST Response From File With Get And Query String
    [Documentation]  Test REST response from file with GET method and query string
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response From File  path=test  method=GET  filePath=${FILE_PATH}  queryParameters=&{QUERY_PARAMS}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Get Request  client  /test?param1=value1&param2=value2
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  GET
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.queryString}  param1=value1&param2=value2
    
Test REST Response With Post
    [Documentation]  Test REST response with POST method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response  path=test  method=POST  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Post Request  client  uri=/test  data=${REQUEST_CONTENT}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}
    
Test REST Response From File With Post
    [Documentation]  Test REST response from file with POST method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response From File  path=test  method=POST  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Post Request  client  uri=/test  data=${REQUEST_CONTENT}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  /test
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}
    
Test REST Response With Put
    [Documentation]  Test REST response with PUT method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response  path=test/id  method=PUT  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Put Request  client  uri=/test/id  data=${REQUEST_CONTENT}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  PUT
    Should Be Equal  ${request.path}  /test/id
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}
    
Test REST Response From File With Put
    [Documentation]  Test REST response from file with PUT method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response From File  path=test/id  method=PUT  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Put Request  client  uri=/test/id  data=${REQUEST_CONTENT}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  PUT
    Should Be Equal  ${request.path}  /test/id
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}
    
Test REST Response With Delete
    [Documentation]  Test REST response with DELETE method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response  path=test/id  method=DELETE  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Delete Request  client  /test/id
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  DELETE
    Should Be Equal  ${request.path}  /test/id
    
Test REST Response From File With Delete
    [Documentation]  Test REST response from file with DELETE method
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set REST Response From File  path=test/id  method=DELETE  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{HEADERS}
    ${response} =  Delete Request  client  /test/id
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  DELETE
    Should Be Equal  ${request.path}  /test/id