*** Settings ***
resource        ../resources/common.robot
Suite Setup     Setup Test Suite
Suite Teardown  Teardown Test Suite

*** Variables ***
${CONTENT}    test content
&{PARAMS}     param1=value1  param2=value2
&{HEADERS}    Content-Type=text/plain  header2=value2

*** Test Cases ***
Test Requests
    [Documentation]  Test requests data with multiple requests
    [Tags]  Unit
    [Setup]  Setup Test Case
    Get Request  client  /test1  params=&{PARAMS}  headers=&{HEADERS}
    Post Request  client  /test2  data=${CONTENT}  params=&{PARAMS}  headers=&{HEADERS}
    Put Request  client  /test3  data=${CONTENT}  params=&{PARAMS}  headers=&{HEADERS}
    Delete Request  client  /test4  params=&{PARAMS}  headers=&{HEADERS}
    ${request1} =  Get HTTP Request  ${1}
    Should Be Equal  ${request1.method}  GET
    Should Be Equal  ${request1.path}  /test1
    Should Be Equal  ${request1.queryString}  param1=value1&param2=value2
    Dictionary Should Contain Key  ${request1.headers}  Content-Type
    Dictionary Should Contain Key  ${request1.headers}  header2
    ${request2} =  Get HTTP Request  ${2}
    Should Be Equal  ${request2.method}  POST
    Should Be Equal  ${request2.path}  /test2
    Should Be Equal  ${request2.contentType}  text/plain
    Should Be Equal  ${request2.body}  ${CONTENT}
    Should Be Equal  ${request2.queryString}  param1=value1&param2=value2
    Dictionary Should Contain Key  ${request2.headers}  Content-Type
    Dictionary Should Contain Key  ${request2.headers}  header2  
    ${request3} =  Get HTTP Request  ${3}
    Should Be Equal  ${request3.method}  PUT
    Should Be Equal  ${request3.path}  /test3
    Should Be Equal  ${request3.contentType}  text/plain
    Should Be Equal  ${request3.body}  ${CONTENT}
    Should Be Equal  ${request3.queryString}  param1=value1&param2=value2
    Dictionary Should Contain Key  ${request3.headers}  Content-Type
    Dictionary Should Contain Key  ${request3.headers}  header2
    ${request4} =  Get HTTP Request
    Should Be Equal  ${request4.method}  DELETE
    Should Be Equal  ${request4.path}  /test4
    Should Be Equal  ${request4.queryString}  param1=value1&param2=value2
    Dictionary Should Contain Key  ${request4.headers}  Content-Type
    Dictionary Should Contain Key  ${request4.headers}  header2