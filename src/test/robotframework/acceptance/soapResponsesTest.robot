*** Settings ***
resource        ../resources/common.robot
Suite Setup     Setup Test Suite
Suite Teardown  Teardown Test Suite

*** Variables ***
${PATH}                   /testService
${SOAP_ACTION}            soapAction
${REQUEST_CONTENT}        <?xml version="1.0"?><soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope/" soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding"><soap:Body><Request><Element>101</Element></Request></soap:Body></soap:Envelope>
${RESPONSE_CONTENT}       <?xml version="1.0"?><soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope/" soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding"><soap:Body><Response><Element>101</Element></Response></soap:Body></soap:Envelope>
${CONTENT_TYPE}           application/soap+xml
${FILE_PATH}              ${CURDIR}/../resources/soapContent.xml
${FILE_CONTENT}           <?xml version="1.0"?><soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope/" soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding"><soap:Body><Response><Element>101</Element></Response></soap:Body></soap:Envelope>
&{REQUEST_HEADERS}        SOAPAction=${SOAP_ACTION}
&{RESPONSE_HEADERS}       header1=value1  header2=value2

*** Test Cases ***
Test SOAP Response
    [Documentation]  Test SOAP response
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set SOAP Response  path=${PATH}  soapAction=${SOAP_ACTION}  content=${RESPONSE_CONTENT}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{RESPONSE_HEADERS}
    ${response} =  Post Request  client  uri=${PATH}  data=${REQUEST_CONTENT}  headers=&{REQUEST_HEADERS}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${RESPONSE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  ${PATH}
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}
    
Test SOAP Response From File
    [Documentation]  Test SOAP response from file
    [Tags]  Unit
    [Setup]  Reset Mock Server
    Set SOAP Response From File  path=${PATH}  soapAction=${SOAP_ACTION}  filePath=${FILE_PATH}  statusCode=${200}  contentType=${CONTENT_TYPE}  headers=&{RESPONSE_HEADERS}
    ${response} =  Post Request  client  uri=${PATH}  data=${REQUEST_CONTENT}  headers=&{REQUEST_HEADERS}
    Should Be Equal  ${response.status_code}  ${200}
    Should Be Equal  ${response.content}  ${FILE_CONTENT}
    Dictionary Should Contain Key  ${response.headers}  header1
    Dictionary Should Contain Key  ${response.headers}  header2
    ${request} =  Get HTTP Request
    Should Be Equal  ${request.method}  POST
    Should Be Equal  ${request.path}  ${PATH}
    Should Be Equal  ${request.body}  ${REQUEST_CONTENT}