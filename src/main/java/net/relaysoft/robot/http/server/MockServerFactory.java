package net.relaysoft.robot.http.server;

/**
 * Factory for creating new {@link MockServer mock servers}.
 * 
 * @author relaysoft.net
 *
 */
public final class MockServerFactory {
	
	private MockServerFactory(){}
	
	/**
	 * @return New mock server instance.
	 */
	public static MockServer getMockServer(){
		return new MockServer();
	}
}
