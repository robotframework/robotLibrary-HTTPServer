package net.relaysoft.robot.http.server;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class ResponseDataHolder {
	
	private String body;
	private String contentType;
	
	private Integer code;
	
	private Map<String, String> headers;
	
	private Path filePath;

	public ResponseDataHolder(String body, String contentType, Integer code, Map<String, String> headers) {
		super();
		this.body = body;
		this.contentType = contentType;
		this.code = code;
		this.headers = headers != null ? headers : new HashMap<>();
	}
	
	public ResponseDataHolder(Path filePath, String contentType, Integer code, Map<String, String> headers) {
		super();
		this.filePath = filePath;
		this.contentType = contentType;
		this.code = code;
		this.headers = headers != null ? headers : new HashMap<>();
	}
	
	@Override
	public String toString() {
		return "ResponseDataHolder [body=" + body + ", contentType=" + contentType + ", code=" + code + ", headers=" + headers
				+ ", filePath=" + filePath + "]";
	}

	public String getBody() {
		return body;
	}	

	public String getContentType() {
		return contentType;
	}

	public Integer getCode() {
		return code;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}
	
	public Path getFilePath() {
		return filePath;
	}
	
	
}
