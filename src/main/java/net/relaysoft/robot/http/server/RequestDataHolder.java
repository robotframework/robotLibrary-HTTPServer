package net.relaysoft.robot.http.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.relaysoft.robot.http.server.utils.HTTPUtils;
import net.relaysoft.robot.http.server.utils.StringUtils;

public class RequestDataHolder {
	
	private static final String HEADER_CONTENT_TYPE = "Content-Type";

	private String authType;
	private String body;
	private String contentType;
	private String encoding;
	private String method;
	private String queryString;
	private String path;
	
	private Map<String, String> headers;
	
	public RequestDataHolder(HttpServletRequest httpServletRequest) throws IOException {
		authType = HTTPUtils.getAuthType(httpServletRequest);
		body = convertRequestBodyToString(httpServletRequest); 
		contentType = httpServletRequest.getContentType();
		encoding = httpServletRequest.getCharacterEncoding();
		method = httpServletRequest.getMethod();
		queryString = httpServletRequest.getQueryString(); 
		path = httpServletRequest.getPathInfo();
		headers = convertRequestHeadersToMap(httpServletRequest);
	}
	
	@Override
	public String toString() {
		return "RequestDataHolder [authType=" + authType + ", body=" + body + ", contentType=" + contentType + ", encoding=" + encoding
				+ ", method=" + method + ", queryString=" + queryString + ", path=" + path + ", headers=" + headers + "]";
	}

	public String getAuthType() {
		return authType;
	}

	public String getBody() {
		return body;
	}
	
	public String getContentType() {
		return StringUtils.isBlank(contentType) && headers.containsKey(HEADER_CONTENT_TYPE) ? headers.get(HEADER_CONTENT_TYPE) : contentType;
	}

	public String getEncoding() {
		return encoding;
	}

	public String getMethod() {
		return method;
	}

	public String getQueryString() {
		return queryString;
	}

	public String getPath() {
		return path;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}
	
	/**
	 * Converts HTTP requests body into string. 
	 * 
	 * @param httpServletRequest - HTTP request
	 * @return Body as string.
	 * @throws IOException If reading or writing the body content fails.
	 */
	private String convertRequestBodyToString(HttpServletRequest httpServletRequest) throws IOException{
		String charset = (httpServletRequest.getCharacterEncoding() != null ?
				httpServletRequest.getCharacterEncoding() :
					HTTPUtils.getCharsetFromContentType(httpServletRequest.getContentType()));
		try(InputStream is = httpServletRequest.getInputStream()){
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			return result.toString(charset);
		}
	}
	
	/**
	 * Converts HTTP request headers into key value map.
	 * 
	 * @param httpServletRequest - HTTP request
	 * @return Map of HTTP headers where header name is the key.
	 */
	private Map<String, String> convertRequestHeadersToMap(HttpServletRequest httpServletRequest){
		Map<String, String> convertedHeaders = new HashMap<>();
		Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String name = headerNames.nextElement();
			String value = "";
			Enumeration<String> requestHeaders = httpServletRequest.getHeaders(name);
			while(requestHeaders.hasMoreElements()){
				value = value.isEmpty() ? requestHeaders.nextElement() : value.concat(" ").concat(requestHeaders.nextElement());
			}
			convertedHeaders.put(name, value);
		}
		return convertedHeaders;
	}
		
}
