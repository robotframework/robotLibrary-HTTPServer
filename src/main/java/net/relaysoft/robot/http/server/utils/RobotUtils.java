package net.relaysoft.robot.http.server.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Robot framework utility methods.
 * 
 * @author relaysoft.net
 *
 */
public final class RobotUtils {

	private RobotUtils() {}
	
	/**
	 * Get optional parameter value from given parameter array.
	 * 
	 * @param params - Parameter array
	 * @param index - Parameter index to return
	 * @param defaultValue - Default value to return if parameter does not exists in given index or its value is <code>null</code>.
	 * @param <T> This is the type parameter for the value
	 * @return Return parameter value or default value. Value can be Character sequence, number or dictionary.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getParameterValue(String[] params, int index, T defaultValue){
		T resultValue = null;
		String value = null;
		if (params.length > index) {
			value = params[index];
		}
		if (StringUtils.isNotBlank(value) && !"None".equals(value)) {
			if(isDictionary(value)){
				resultValue = (T) parseDictionary(value);
			} else if(StringUtils.isNumeric(value)){
				resultValue = (T) new Integer(value);
			} else if(defaultValue == null || defaultValue instanceof String) {
				resultValue = (T) value;
			}
		}
		return resultValue != null ? resultValue : defaultValue;
	}

	/**
	 * Checks whether string value structure equals to dictionary value.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string value equals to dictionary structure.
	 */
	public static boolean isDictionary(String string) {
		boolean result = false;
		if(string != null && !string.isEmpty() && string.trim().startsWith("{") && string.trim().endsWith("}")){
			String subString = StringUtils.substringBetween(string, "{", "}");
			String[] pairs = subString.split(",");
			for(String pair : pairs){
				String[] values = pair.split(":");
				if(values.length == 2 && values[0].trim().startsWith("u'") && values[0].trim().endsWith("'") 
						&& values[1].trim().startsWith("u'") && values[1].trim().endsWith("'")){
					result = true;
				} else {				
					return false;
				}
			}
		}
		return result;
	}
	
	/**
	 * Parser given string value into Map (dictionary).
	 * 
	 * @param string - String value to parse
	 * @return Map parsed from given string.
	 */
	public static Map<String, String> parseDictionary(String string){
		Map<String, String> map = new HashMap<>();
		String subString = StringUtils.substringBetween(string, "{", "}");
		String[] pairs = subString.split(",");
		for(String pair : pairs){
			String[] values = pair.split(":");
			map.put(StringUtils.substringBetween(values[0].trim(), "'", "'"), StringUtils.substringBetween(values[1].trim(), "'", "'"));
		}
		return map;
	}

}
