package net.relaysoft.robot.http.server.utils;

/**
 * Utility class for string operations
 * 
 * @author relaysoft.net
 *
 */
public final class StringUtils {

	private StringUtils(){}

	/**
	 * Checks whether string is either <code>null</code> or empty.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string is <code>null</code> or empty.
	 */
	public static boolean isBlank(String string) {
		return string == null || string.trim().isEmpty();
	}

	/**
	 * Checks that string is not <code>null</code> or empty.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string is not <code>null</code> or empty.
	 */
	public static boolean isNotBlank(String string) {
		return string != null && !string.trim().isEmpty();
	}
	
	/**
	 * Checks whether string only contains numeric characters.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string contains only numeric characters.
	 */
	public static boolean isNumeric(String string) {
	  return string.matches("-?\\d+(\\.\\d+)?");
	}

	/**
	 * Return sub string after given character sequence from original string.
	 * 
	 * @param str - Original string
	 * @param after - Character sequence after which sub string is returned
	 * @return Sub string or empty string if given after character sequence is not found. 
	 */
	public static String substringAfter(String str, String after) {
		if (isBlank(str)) {
			return str;
		}
		if (after == null) {
			return "";
		}
		int position = str.indexOf(after);
		if (position == -1) {
			return "";
		}
		return str.substring(position + after.length());
	}
	
	/**
	 * Return sub string between given character sequences from original string.
	 * 
	 * @param str - Original string
	 * @param after - Character sequence after which sub string is returned
	 * @param before - Character sequence before which sub string is returned
	 * @return Sub string or empty string if given after and before character sequences are not found.
	 */
	public static String substringBetween(String str, String after, String before) {
		if (isBlank(str)) {
			return str;
		}
		if (after == null || before == null) {
			return "";
		}
		int afterPosition = str.indexOf(after);
		int beforePosition = str.indexOf(before, afterPosition + 1);
		if (afterPosition == -1 || beforePosition == -1) {
			return "";
		}
		return str.substring(afterPosition + 1, beforePosition);
	}
}
