package net.relaysoft.robot.http.server.utils;

public final class PrintUtil {
	
	private PrintUtil() {}

	/**
	 * Print INFO into robot log output.
	 * 
	 * @param string - Value to print
	 */
	public static void printOut(String string){
		if(StringUtils.isNotBlank(string)){
			System.out.println(string);
		}
	}

}
