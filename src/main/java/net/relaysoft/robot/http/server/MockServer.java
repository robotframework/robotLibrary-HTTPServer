package net.relaysoft.robot.http.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import net.relaysoft.robot.http.server.utils.HTTPUtils;
import net.relaysoft.robot.http.server.utils.StringUtils;

import static javax.servlet.http.HttpServletResponse.SC_OK;

/**
 * Mock HTTP server used for storing configured mapped responses and requests made against it. 
 * 
 * @author relaysoft.net
 *
 */
public class MockServer {

	private static final String DEFAULT = "DEFAULT";
	private static final String DEFAULT_CONTENT_TYPE = "text/plain";

	private static final String HEADER_SOAP_ACTION = "SOAPAction";
	private static final String REQUEST_METHOD_POST = "POST";

	private static final MultiKey DEFAULT_RESPONSE_KEY = new MultiKey(DEFAULT, DEFAULT, DEFAULT, DEFAULT);

	private Server server;

	private int port = 0;

	private List<RequestDataHolder> requests;

	private Map<MultiKey, ResponseDataHolder> responses;


	/**
	 * Create new mock server instance.
	 */
	MockServer(){
		requests = new ArrayList<>();
		responses = new HashMap<>();
	}

	/**
	 * Initialize mock server.
	 *
	 * @param port - Port to listen when started. If 0 then first available port is selected
	 * @throws IOException - If selecting the first available port fails.
	 */
	public void initializeServer(int port) throws IOException {
		this.port = port > 0 ? port : getAvailablePort();
		server = new Server(this.port);
		server.setHandler(createMockHandler());
	}

	/**
	 * Start mock server
	 *
	 * @throws Exception - If starting fails.
	 */
	public void start() throws Exception {
		server.start();
	}

	/**
	 * Stop mock server.
	 *
	 * @throws Exception - If stopping fails.
	 */
	public void stop() throws Exception {
		if(server != null) {
			server.stop();
		}
	}

	/**
	 * @return Port which mock server is listening.
	 */
	public int getPort(){
		return port;
	}
	
	/**
	 * @return Request data holder from the latest request after initialize or reset.
	 */
	public RequestDataHolder getRequest(){
		return getRequest(requests.size());
	}

	/**
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return Request data holder from the given request after initialize or reset.
	 */
	public RequestDataHolder getRequest(int requestNumber){
		if(!requests.isEmpty()){
			try{
				return requests.get(requestNumber - 1);
			} catch (ArrayIndexOutOfBoundsException e){
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * @return Authentication type from the latest request after initialize or reset.
	 */
	public String getRequestAuthType(){
		return getRequestAuthType(requests.size());
	}

	/**
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return Authentication type from the given request after initialize or reset.
	 */
	public String getRequestAuthType(int requestNumber){
		if(!requests.isEmpty()){
			try{
				return requests.get(requestNumber - 1).getAuthType();
			} catch (ArrayIndexOutOfBoundsException e){
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * @return Request body from the latest request after initialize or reset.
	 */
	public String getRequestBody() {
		return getRequestBody(requests.size());
	}

	/**
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return Request body from the given request after initialize or reset.
	 */
	public String getRequestBody(int requestNumber) {
		if(!requests.isEmpty()) {
			try {
				return requests.get(requestNumber - 1).getBody();
			} catch (ArrayIndexOutOfBoundsException e) {
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * @return Number of request made against this server since last reset.
	 */
	public int getRequestCount() {
		return requests.size();
	}

	/**
	 * @return All request headers from latest request after initialize or reset.
	 */
	public Map<String, String> getRequestHeaders(){
		return getRequestHeaders(requests.size());
	}

	/**
	 *
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return All request headers from given request after initialize or reset.
	 */
	public Map<String, String> getRequestHeaders(int requestNumber){
		if(!requests.isEmpty()) {
			try {
				return requests.get(requestNumber - 1).getHeaders();
			} catch (ArrayIndexOutOfBoundsException e) {
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * @return Request method from latest request after initialize or reset.
	 */
	public String getRequestMethod() {
		return getRequestMethod(requests.size());
	}

	/**
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return Request method from given request after initialize or reset.
	 */
	public String getRequestMethod(int requestNumber){
		if(!requests.isEmpty()){
			try{
				return requests.get(requestNumber - 1).getMethod();
			} catch (ArrayIndexOutOfBoundsException e){
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * @return Request query string from latest request after initialize or reset.
	 */
	public String getRequestQueryString(){
		return getRequestQueryString(requests.size());
	}

	/**
	 * @param requestNumber - Number of request to check after initialize or reset
	 * @return Request query string from given request after initialize or reset.
	 */
	public String getRequestQueryString(int requestNumber){
		if(!requests.isEmpty()){
			try{
				return requests.get(requestNumber - 1).getQueryString();
			} catch (ArrayIndexOutOfBoundsException e){
				// It is OK to return null when there is no request data available.
			}
		}
		return null;
	}

	/**
	 * Reset all saved data from requests made against this mock.
	 */
	public void resetRequestData(){
		requests.clear();
	}

	/**
	 * Reset all set response values back to defaults.
	 */
	public void resetResponseData(){
		responses.clear();
	}

	/**
	 * Sets default response to return in case when request URL, method, query string and/or SOAP action does not match any REST or SOAP
	 * response configured into this mock server.
	 * 
	 * @param body - Response body content. Can be <code>null</code>, default is empty response
	 * @param contentType - Response body content type. Can be <code>null</code>, default is content type from request
	 * @param code - Response HTTP status code, Can be <code>null</code>, default is 200.
	 * @param headers - Response headers, Can be <code>null</code>.
	 */
	public void setDefaultResponse(String body, String contentType, Integer code, Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(body, contentType, code, headers);
		responses.put(DEFAULT_RESPONSE_KEY, response);
	}

	/**
	 * Sets default response to return in case when request URL, method, query string and/or SOAP action does not match any REST or SOAP
	 * response configured into this mock server. Response body content is retrieved from file.
	 * 
	 * @param filePath - Path to file which content is used for response body content
	 * @param contentType - Response body content type. Can be <code>null</code>, default is content type from request
	 * @param code - Response HTTP status code, Can be <code>null</code>, default is 200.
	 * @param headers - Response headers, Can be <code>null</code>.
	 */
	public void setDefaultResponseFromFile(String filePath, String contentType, Integer code, Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(Paths.get(filePath), contentType, code, headers);
		responses.put(DEFAULT_RESPONSE_KEY, response);
	}

	/**
	 * Sets REST response to return in case when request URL, method, and optional query string matches into given values.
	 * 
	 * @param path - Request URL path to match
	 * @param method - Request method to match
	 * @param queryParameters - Optional request query parameters to match
	 * @param body - Body content to return in response
	 * @param contentType - Response body content type
	 * @param code - Response HTTP code
	 * @param headers - Response HTTP headers
	 */
	public void setRestResponseData(String path, String method, Map<String, String> queryParameters, String body, String contentType, 
			Integer code, Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(body, contentType, code, headers);
		MultiKey responseKey = createMultiKey(path, method, queryParameters, null);
		responses.put(responseKey, response);
	}

	/**
	 * Sets REST response to return in case when request URL, method, and optional query string matches into given values. 
	 * Response body content is retrieved from file.
	 * 
	 * @param path - Request URL path to match
	 * @param method - Request method to match
	 * @param queryParameters - Optional request query parameters to match
	 * @param filePath - Path to file which content is used for response body content
	 * @param contentType - Response body content type
	 * @param code - Response HTTP code
	 * @param headers - Response HTTP headers
	 */
	public void setRestResponseDataFromFile(String path, String method, Map<String, String> queryParameters, String filePath, 
			String contentType, Integer code, Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(Paths.get(filePath), contentType, code, headers);
		MultiKey responseKey = createMultiKey(path, method, queryParameters, null);
		responses.put(responseKey, response);
	}

	/**
	 * Sets REST response to return in case when request URL, and SOAP action matches into given values. 
	 * 
	 * @param path - Request URL path to match
	 * @param soapAction - SOAP action to match
	 * @param body - Body content to return in response
	 * @param contentType - Response body content type
	 * @param code - Response HTTP code
	 * @param headers - Response HTTP headers
	 */
	public void setSoapResponseData(String path, String soapAction, String body, String contentType, Integer code, 
			Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(body, contentType, code, headers);
		MultiKey responseKey = createMultiKey(path, REQUEST_METHOD_POST, null, soapAction);
		responses.put(responseKey, response);
	}

	/**
	 * Sets REST response to return in case when request URL, and SOAP action matches into given values.
	 * Response body content is retrieved from file.
	 * 
	 * @param path - Request URL path to match
	 * @param soapAction - SOAP action to match
	 * @param filePath - Path to file which content is used for response body content
	 * @param contentType - Response body content type
	 * @param code - Response HTTP code
	 * @param headers - Response HTTP headers
	 */
	public void setSoapResponseDataFromFile(String path, String soapAction, String filePath, String contentType, Integer code, 
			Map<String, String> headers){
		ResponseDataHolder response = new ResponseDataHolder(Paths.get(filePath), contentType, code, headers);
		MultiKey responseKey = createMultiKey(path, REQUEST_METHOD_POST, null, soapAction);
		responses.put(responseKey, response);
	}

	/**
	 * Create new mock servlet handler to process incoming requests.
	 *
	 * @return Mock handler.
	 */
	private Handler createMockHandler() {
		return new AbstractHandler(){
			@Override
			public void handle(String target, Request request, HttpServletRequest httpServletRequest,
					HttpServletResponse httpServletResponse) throws IOException, ServletException {
				RequestDataHolder requestData = createRequestDataHolder(httpServletRequest);
				requests.add(requestData);
				ResponseDataHolder responseData = getResponseForRequest(requestData);
				initializeResponse(httpServletResponse, responseData, requestData);
				request.setHandled(true);
			}
		};
	}

	/**
	 *  Find and return first available port form current server.
	 *
	 * @return Available port.
	 * @throws IOException If opening server fails.
	 */
	private int getAvailablePort() throws IOException {
		try(ServerSocket socket = new ServerSocket(0)) {
			return socket.getLocalPort();
		}
	}

	/**
	 * Initializes HTTP servlet response to ready for returned back to requester.
	 * 
	 * @param httpServletResponse - HTTP servlet request object
	 * @param responseData - Response data holder used for initialization source
	 * @param requestData - Request data holder from original HTTP request
	 * @throws IOException If writing response body content fails.
	 */
	private void initializeResponse(HttpServletResponse httpServletResponse, ResponseDataHolder responseData, RequestDataHolder requestData) 
			throws IOException{
		String contentType = responseData.getContentType() != null ? responseData.getContentType() : requestData.getHeaders().get("Accept");
		httpServletResponse.setContentType(contentType != null ? contentType : DEFAULT_CONTENT_TYPE);
		httpServletResponse.setStatus(responseData.getCode() != null ? responseData.getCode() : SC_OK);
		responseData.getHeaders().entrySet().stream().forEach(e -> httpServletResponse.setHeader(e.getKey(), e.getValue()));
		writeResponseBody(httpServletResponse, responseData);
	}

	/**
	 * Create request data holder from HTTP request.
	 * 
	 * @param httpServletRequest - HTTP request.
	 * @return Request data holder.
	 * @throws IOException If getting body content from HTTP request fails.
	 */
	private RequestDataHolder createRequestDataHolder(HttpServletRequest httpServletRequest) throws IOException{
		return new RequestDataHolder(httpServletRequest);
	}

	/**
	 * Resolves which response data holder should be used for HTTP response generation against given request.
	 * 
	 * @param requestData - Request data holder created from original HTTP request.
	 * @return Response data holder or default response data if request did not match any configured responses.
	 */
	private ResponseDataHolder getResponseForRequest(RequestDataHolder requestData){
		MultiKey responseKey = createMultiKey(requestData);
		ResponseDataHolder response = responses.get(responseKey);
		if(response == null){
			response = responses.get(DEFAULT_RESPONSE_KEY);
		}
		if(response == null){
			response = generateDefaultResponse();
		}
		return response;
	}

	/**
	 * @return Default response data holder.
	 */
	private ResponseDataHolder generateDefaultResponse(){
		return new ResponseDataHolder((String) null, (String) null, HttpServletResponse.SC_NOT_FOUND, new HashMap<>());
	}

	/**
	 * Create key from request data to find matching response from map.
	 * 
	 * @param requestData - Request data holder
	 * @return Key generated from request data.
	 */
	private MultiKey createMultiKey(RequestDataHolder requestData){
		String path = requestData.getPath();
		String method = requestData.getMethod();
		Map<String, String> queryParams = HTTPUtils.convertQueryStringIntoMap(requestData.getQueryString());
		String soapAction = requestData.getHeaders().get(HEADER_SOAP_ACTION);
		return createMultiKey(path, method, queryParams, soapAction);
	}

	/**
	 * Create new multi key from path, query string, and soap action values. Values can be null in which case 'default'
	 * value is used in key.
	 *
	 * @param path - Path value
	 * @param queryString - Query string value
	 * @param soapAction - Soap action value
	 * @return
	 */
	private MultiKey createMultiKey(String path, String method, Map<String, String> queryParams, String soapAction){
		return new MultiKey((StringUtils.isNotBlank(path) ? populatePath(path) : DEFAULT),
				(StringUtils.isNotBlank(method) ? method : DEFAULT),
				(queryParams != null ? queryParams : new HashMap<>()),
				(StringUtils.isNotBlank(soapAction) ? soapAction : DEFAULT));
	}
	
	/**
	 * Populates URL path value with leading '/' character if needed.
	 * 
	 * @param path - Path to populate
	 * @return Populated URL path
	 */
	private String populatePath(String path) {
		return path.startsWith("/") ? path : "/".concat(path);
	}

	/**
	 * Writes body content into HTTP response from response data holder.
	 * 
	 * @param httpServletResponse - HTTP response object
	 * @param responseData - Response data holder
	 * @throws IOException If writing the data fails.
	 */
	private void writeResponseBody(HttpServletResponse httpServletResponse, ResponseDataHolder responseData) throws IOException{
		String body = responseData.getBody();
		if(StringUtils.isNotBlank(body)){
			String charset = HTTPUtils.getCharsetFromContentType(httpServletResponse.getContentType());
			byte[] data = body.getBytes(charset);
			try(OutputStream os = httpServletResponse.getOutputStream()){
				os.write(data);
			}
		} else if(responseData.getFilePath() != null){
			Files.copy(responseData.getFilePath(), httpServletResponse.getOutputStream());
		}
	}
}
