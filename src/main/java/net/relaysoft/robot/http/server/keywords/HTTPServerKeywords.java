package net.relaysoft.robot.http.server.keywords;

import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywordOverload;
import org.robotframework.javalib.annotation.RobotKeywords;

import net.relaysoft.robot.http.server.MockServer;
import net.relaysoft.robot.http.server.MockServerFactory;
import net.relaysoft.robot.http.server.RequestDataHolder;
import net.relaysoft.robot.http.server.utils.PrintUtil;
import net.relaysoft.robot.http.server.utils.RobotUtils;
import net.relaysoft.robot.http.server.utils.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RobotKeywords
public class HTTPServerKeywords {
	
	private static final String DEFAULT_REST_METHOD = "GET";
	private static final String DEFAULT_SOAP_CONTENT_TYPE = "application/soap+xml";
	
	private static final int DEFAULT_STATUS_CODE = 200;
	
	private static final String NONE = "None";
	
	private static final String PARAM_CODE = "statusCode";
	private static final String PARAM_CONTENT = "content";
	private static final String PARAM_FILE = "filePath";
	private static final String PARAM_HEADERS = "headers";
	private static final String PARAM_PATH = "path";
	private static final String PARAM_TYPE = "contentType";

    private MockServer server;
    
    @RobotKeyword("Return request data holder from request made against HTTP server. Request data holder contain all recorded request "
    		+ "information from single request. Following values can be retrieved from request data holder.\n"
    		+ "_authType_, _body_, _encoding_, _contentType_, _method_, _path_, and _queryString_.\n\n" +
            "Arguments:\n" +
            "- _requestNumber_: Sequence number of the request from which request body should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request | |\n" +
            "| Get HTTP Request | ${2} |")
    @ArgumentNames({"requestNumber="})
    public RequestDataHolder getHTTPRequest(int requestNumber) {
        return server.getRequest(requestNumber);
    }
    
    @RobotKeywordOverload
    public RequestDataHolder getHTTPRequest(){ 
    	return server.getRequest(); 
    }

    @RobotKeyword("Return request body from request made against HTTP server.\n\n" +
            "Arguments:\n" +
            "- _requestNumber_: Sequence number of the request from which request body should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request Content | |\n" +
            "| Get HTTP Request Content | ${2} |")
    @ArgumentNames({"requestNumber="})
    public String getHTTPRequestContent(int requestNumber) {
        return server.getRequestBody(requestNumber);
    }

    @RobotKeywordOverload
    public String getHTTPRequestContent(){ 
    	return server.getRequestBody(); 
    }

    @RobotKeyword("Return single request header value from request made against HTTP server.\n\n" +
            "Arguments:\n" +
            "- _name_: Header name which value should be returned.\n" +
            "- _requestNumber_: Sequence number of the request from which header should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request Header | Content-Type |\n" +
            "| Get HTTP Request Header | Content-Type | ${2} |")
    @ArgumentNames({"name", "requestNumber="})
    public String getHTTPRequestHeader(String name, int requestNumber) {
        return server.getRequestHeaders(requestNumber).get(name);
    }

    @RobotKeywordOverload
    public String getHTTPRequestHeader(String name) {
        return server.getRequestHeaders().get(name);
    }

    @RobotKeyword("Return all request headers as a dictionary from request made against HTTP server.\n\n" +
            "Arguments:\n" +
            "- _requestNumber_: Sequence number of the request from which headers should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request Headers | |\n" +
            "| Get HTTP Request Headers | ${2} |")
    @ArgumentNames({"requestNumber="})
    public Map<String, String> getHTTPRequestHeaders(int requestNumber) {
        return server.getRequestHeaders(requestNumber);
    }

    @RobotKeywordOverload
    public Map<String, String> getHTTPRequestHeaders() {
        return server.getRequestHeaders();
    }

    @RobotKeyword("Return request method from request made against HTTP server.\n\n" +
            "Arguments:\n" +
            "- _requestNumber_: Sequence number of the request from which request method should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request Method | |\n" +
            "| Get HTTP Request Method | ${2} |")
    @ArgumentNames({"requestNumber="})
    public String getHTTPRequestMethod(int requestNumber){
        return server.getRequestMethod(requestNumber);
    }

    @RobotKeywordOverload
    public String getHTTPRequestMethod(){
        return server.getRequestMethod();
    }

    @RobotKeyword("Return request query string from request made against HTTP server.\n\n" +
            "Arguments:\n" +
            "- _requestNumber_: Sequence number of the request from which request method should be returned."
            + "If sequence number is not given then value is retrieved from latest request made against server.\n\n" +
            "Examples:\n" +
            "| Get HTTP Request Query String | |\n" +
            "| Get HTTP Request Query String | ${2} |")
    @ArgumentNames({"requestNumber="})
    public String getHTTPRequestQueryString(int requestNumber){
        return server.getRequestQueryString(requestNumber);
    }

    @RobotKeywordOverload
    public String getHTTPRequestQueryString(){
        return server.getRequestQueryString();
    }

    @RobotKeyword("Return port number which HTTP server is listening.")
    public int getHTTPServerPort(){
        return server.getPort();
    }

    @RobotKeyword("Initializes HTTP server. This keyword must be executed before any others can be used.\n\n" +
            "Arguments:\n" +
            "- _port_: Listening port number. If port value is not given then the first free port is dynamically selected. "
            + "Dynamically selected port can be queried with `Get HTTP Server Port` keyword.\n\n" +
            "Examples:\n" +
            "| Initialize HTTP Server | |" +
            "| Initialize HTTP Server | ${8888} |")
    @ArgumentNames({"port"})
    public void initializeHTTPServer(int port) throws IOException {
    	server = MockServerFactory.getMockServer();
        server.initializeServer(port);
    }
    
    @RobotKeywordOverload
    public void initializeHTTPServer() throws IOException {
    	initializeHTTPServer(0);
    }

    @RobotKeyword("Reset all recorded requests from HTTP server.")
    public void resetHTTPRequestContents(){
        server.resetRequestData();
    }
    
    @RobotKeyword("Reset all configured responses from HTTP server.")
    public void resetHTTPResponseContents(){
        server.resetResponseData();
    }
    
    @RobotKeyword("Set default HTTP response to return for the requests. This response default for all requests which does not have "
    		+ "matching response defined by specified paths, methods, query strings, or SOAP actions.\n\n" +
            "Arguments:\n" +
            "- _content_: Optional response body to return.\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
            "| Set Default Response | content=Hello | | | |\n" +
            "| Set Default Response | content=Hello | statusCode=${201} | contentType=text/plain | |\n" + 
            "| Set Default Response | content=Hello | statusCode=${201} | contentType=text/plain | headers=&{header_dict} |")
    @ArgumentNames({"content=None", "statusCode=200", "contentType=None", "headers={}"})
    public void setDefaultResponse(String... params) {
    	String content = RobotUtils.getParameterValue(params, 0, null);
    	int statusCode = RobotUtils.getParameterValue(params, 1, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 2, null);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 3, new HashMap<>());
    	PrintUtil.printOut(PARAM_CONTENT+"="+content+", "+PARAM_CODE+"="+statusCode+", "+PARAM_TYPE+"="+contentType+
    			", "+PARAM_HEADERS+"="+headers);
        server.setDefaultResponse(content, contentType, statusCode, headers);
    }
    
    @RobotKeyword("Set default HTTP response to return for the requests. This response default for all requests which does not have "
    		+ "matching response defined by specified paths, methods, query strings, or SOAP actions. Response content is taken from"
    		+ "file content.\n\n" +
            "Arguments:\n" +
            "- _filePath_: File path to response body content file.\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
            "| Set Default Response From File | /path/to/file | | | |\n" +
            "| Set Default Response From File | filePath=/path/to/file | statusCode=${201} | contentType=text/plain | |\n" + 
            "| Set Default Response From File | filePath=/path/to/file | statusCode=${201} | contentType=text/plain | headers=&{header_dict} |")
    @ArgumentNames({"filePath", "statusCode=200", "contentType=None", "headers={}"})
    public void setDefaultResponseFromFile(String filePath, String... params) {
    	int statusCode = RobotUtils.getParameterValue(params, 0, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 1, null);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 2, new HashMap<>());
    	PrintUtil.printOut(PARAM_FILE+"="+filePath+", "+PARAM_CODE+"="+statusCode+", "+PARAM_TYPE+"="+contentType+
    			", "+PARAM_HEADERS+"="+headers);
        server.setDefaultResponseFromFile(filePath, contentType, statusCode, headers);
    }
    
    @RobotKeyword("Set new response data to return for the REST requests. It is possible to set multiple different responses for " +
            "different request paths, query parameters and SOAP actions.\n\n" +
            "Arguments:\n" +
            "- _path_: Request path used to match for this response.\n" +
            "- _method_: Request method used to match for this response. GET, POST, PUT, DELETE\n" +
            "- _queryParameters_: Optional query paramters used to match for this response. Given as dictionary.\n" +
            "- _content_: Optional response body to return.\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
    		"| Set REST Response | path=/test | method=GET | content=Hello | | | | |\n" +
            "| Set REST Response | path=/test | method=GET | queryParameters=&{param_dict} | content=Hello | | | |\n" +
            "| Set REST Response | path=/test | method=GET | queryParameters=&{param_dict} | content=Hello | statusCode=${201} | "
            + "contentType=text/plain | headers=&{header_dict} |")
    @ArgumentNames({"path", "method", "queryParameters={}", "content=None", "statusCode=200", "contentType=None", "headers={}"})
    public void setRESTResponse(String path, String method, String... params) {
    	Map<String, String> queryParameters = RobotUtils.getParameterValue(params, 0, new HashMap<>());
    	String content = RobotUtils.getParameterValue(params, 1, null);
    	int statusCode = RobotUtils.getParameterValue(params, 2, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 3, null);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 4, new HashMap<>());
    	PrintUtil.printOut(PARAM_PATH+"="+resolvePath(path)+", method="+resolveMethod(method)+", queryParameters="+queryParameters+
    			", "+PARAM_CONTENT+"="+content+", "+PARAM_CODE+"="+statusCode+", "+PARAM_TYPE+"="+contentType+", "+PARAM_HEADERS+"="+headers);
        server.setRestResponseData(resolvePath(path), resolveMethod(method), queryParameters, content, contentType, statusCode, headers);
    }
    
    @RobotKeyword("Set new response data to return for the REST requests. It is possible to set multiple different responses for " +
            "different request paths, methods and query parameters. Response content is taken from file content.\n\n" +
            "Arguments:\n" +
            "- _path_: Request path used to match for this response.\n" +
            "- _method_: Request method used to match for this response. GET, POST, PUT, DELETE\n" +
            "- _queryParameters_: Optional query paramters used to match for this response. Given as dictionary.\n" +
            "- _filePath_: File path to response body content file.\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
    		"| Set REST Response From File | path=/test | method=GET | filePath=/path/to/file | | | | |\n" +
            "| Set REST Response From File | path=/test | method=GET | filePath=/path/to/file | queryParameters=&{param_dict} | | | |\n" +
            "| Set REST Response From File | path=/test | method=GET | filePath=/path/to/file | queryParameters=&{param_dict} | statusCode=${201} | "
            + "contentType=text/plain | headers=&{header_dict} |")
    @ArgumentNames({"path", "method", "filePath", "queryParameters={}", "statusCode=200", "contentType=None", "headers={}"})
    public void setRESTResponseFromFile(String path, String method, String filePath, String... params) {
    	Map<String, String> queryParameters = RobotUtils.getParameterValue(params, 0, new HashMap<>());
    	int statusCode = RobotUtils.getParameterValue(params, 1, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 2, null);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 3, new HashMap<>());
    	PrintUtil.printOut(PARAM_PATH+"="+resolvePath(path)+", method="+resolveMethod(method)+", queryParameters="+queryParameters+
    			", "+PARAM_FILE+"="+filePath+", "+PARAM_CODE+"="+statusCode+", "+PARAM_TYPE+"="+contentType+", "+PARAM_HEADERS+"="+headers);
        server.setRestResponseDataFromFile(resolvePath(path), resolveMethod(method), queryParameters, filePath, contentType, statusCode, 
        		headers);
    }
    
    @RobotKeyword("Set new response data to return for the SOAP requests. It is possible to set multiple different responses for " +
            "different request service paths and SOAP actions.\n\n" +
            "Arguments:\n" +
            "- _path_: Request path used to match for this response.\n" +
            "- _soapAction_: SOAP action used to match for this response.\n" +
            "- _content_: Optional response body to return.\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
            "| Set SOAP Response | path=/testService | soapAction=testAction | content=${soap_xml} |\n" +
            "| Set SOAP Response | path=/testService | soapAction=testAction | content=${soap_xml} | statusCode=${200} | contentType=text/plain "
            + "| headers=&{header_dict} |")
    @ArgumentNames({"path", "soapAction", "content", "statusCode=200", "contentType=application/soap+xml", "headers={}"})
    public void setSOAPResponse(String path, String soapAction, String content, String... params) {
    	int statusCode = RobotUtils.getParameterValue(params, 0, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 1, DEFAULT_SOAP_CONTENT_TYPE);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 2, new HashMap<>());
    	PrintUtil.printOut(PARAM_PATH+"="+path+", soapAction="+soapAction+", "+PARAM_CONTENT+"="+content+", "+PARAM_CODE+"="+statusCode+
    			", "+PARAM_TYPE+"="+contentType+", "+PARAM_HEADERS+"="+headers);
        server.setSoapResponseData(path, soapAction, content, contentType, statusCode, headers);
    }
    
    @RobotKeyword("Set new response data to return for the SOAP requests from file content. It is possible to set multiple different " +
    		"responses with different service paths and SOAP actions. Response content is taken from file content." +
            "Arguments:\n" +
            "- _path_: Request path used to match for this response.\n" +
            "- _soapAction_: SOAP action used to match for this response.\n" +
            "- _filePath_: File path to response content data\n" +
            "- _statusCode_: HTTP response status code. Default value is 200.\n" +
            "- _contentType_: Optional response content type. Default matches content type from the request.\n" +
            "- _headers_: Optional custom HTTP headers to return with response. Given as dictionary.\n\n" +
            "Examples:\n" +
            "| Set SOAP Response From File | path=/testService | soapAction=testAction | filePath=/path/to/file |\n" +
            "| Set SOAP Response From File | path=/testService | soapAction=testAction | filePath=/path/to/file | statusCode=${200} | contentType=text/plain "
            + "| headers=&{header_dict} |")
    @ArgumentNames({"path", "soapAction", "filePath", "statusCode=200", "contentType=application/soap+xml", "headers={}"})
    public void setSOAPResponseFromFile(String path, String soapAction, String filePath, String... params) {
    	int statusCode = RobotUtils.getParameterValue(params, 0, DEFAULT_STATUS_CODE);
    	String contentType = RobotUtils.getParameterValue(params, 1, DEFAULT_SOAP_CONTENT_TYPE);
    	Map<String, String> headers = RobotUtils.getParameterValue(params, 2, new HashMap<>());
    	PrintUtil.printOut(PARAM_PATH+"="+path+", soapAction="+soapAction+", "+PARAM_FILE+"="+filePath+", "+PARAM_CODE+"="+statusCode+
    			", "+PARAM_TYPE+"="+contentType+", "+PARAM_HEADERS+"="+headers);
        server.setSoapResponseDataFromFile(path, soapAction, filePath, contentType, statusCode, headers);
    }
    
    @RobotKeyword("Start initialized HTTP mock server")
    public void startHTTPServer() throws Exception {
        server.start();
    }

    @RobotKeyword("Stops running HTTP mock server")
    public void stopHTTPServer() throws Exception {
        server.stop();
    }
    
    private String resolveMethod(String originalMethod) {
    	return StringUtils.isNotBlank(originalMethod) && !NONE.equals(originalMethod) ? originalMethod : DEFAULT_REST_METHOD;
    }
    
    private String resolvePath(String originalPath) {
    	return StringUtils.isNotBlank(originalPath) && !NONE.equals(originalPath) ? originalPath : "/";
    }

}

