package net.relaysoft.robot.http.server.utils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

/**
 * HTTP Utility methods for operating request data.
 * 
 * @author relaysoft.net
 *
 */
public final class HTTPUtils {
	
	protected static final String HEADER_AUTHORIZATION = "Authorization";
	
	private HTTPUtils() {}
	
	/**
	 * Convert query string into map.
	 * 
	 * @param queryString - Query string to convert
	 * @return Query string converted as query parameter map.
	 */
	public static Map<String, String> convertQueryStringIntoMap(String queryString){
		Map<String, String> result = new HashMap<>();
		if(StringUtils.isNotBlank(queryString)){
			result = Arrays.asList(queryString.split("&")).stream()
					.map(s -> s.split("="))
					.collect(Collectors.toMap(e -> e[0], e -> e[1]));
		}
		return result;
	}
	
	/**
	 * Resolves authentication type from HTTP servlet request.
	 * 
	 * @param httpServletRequest - HTTP request object from which authentication type is resolved from
	 * @return Authentication type like BASIC or <code>null</code> if cannot be resolved from request.
	 */
	public static String getAuthType(HttpServletRequest httpServletRequest){
		String authType = httpServletRequest.getAuthType();
		if(authType == null){
			String header = httpServletRequest.getHeader(HEADER_AUTHORIZATION);
			authType = header != null ? header.split(" ")[0] : null;
		}
		return authType;
	}
	
	/**
	 * Resolve character set encoding from Content-Type header value.
	 *
	 * @param contentType - Content type header value
	 * @return Character set encoding name resolved from Content-Type header value or <code>UTF-8</code> if value cannot be resolved.
	 */
	public static String getCharsetFromContentType(String contentType){
		return contentType != null && contentType.contains("charset=") ?
				StringUtils.substringAfter(contentType, "charset=") : StandardCharsets.UTF_8.toString();
	}
}
