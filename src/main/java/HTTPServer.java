import org.robotframework.javalib.library.AnnotationLibrary;

public class HTTPServer extends AnnotationLibrary {
	
	public static final String ROBOT_LIBRARY_SCOPE = "TEST SUITE";
    public static final String ROBOT_LIBRARY_VERSION = "1.0.0";

    private static final String INTRO = "Robot Framework test library for mocking HTTP server.\n\n" +
            "_HTTPServer_ is a test library for mocking HTTP server operations in order to test HTTP client applications.\n" +
            "Implementation is a wrapper on top of Java [http://www.eclipse.org/jetty/|Jetty] library.\n\n" +
            "The library has the following main usages:\n\n" +
            "- Producing pre-configured responses for calling HTTP client applications\n" +
            "- Collecting request information like content, headers, query parameters, etc from requests made against test library HTTP server.\n\n" +
            "== Initializing new HTTP mock server ==\n\n" +
            "Before test library can be used for testing a HTTP mock server must be initialized and started. All servers\n" +
            "must be initialized with unique port number. If port number argument is given as 0 then library dynamically select\n" +
            "first free port it can find from the server. Selected port can be retrieved with `Get Server Port` keyword.\n\n" +
            "Examples:\n" +
            "| Suite Setup | Initialize HTTP Server | port=8888 |\n" +
            "| Start HTTP Server | | |\n" +
            "| Suite Teardown | Stop HTTP Server | |\n\n" +
            "== Retrieving request data ==\n\n" +
            "Library records all the request made against HTTP mock server. Keywords like `Get HTTP Request Content` can be used\n" +
            "to retrieve information about certain request by giving request sequence number as argument for the keyword. Requests\n" +
            "are recorded so that first request can be retrieved with argument value of 1 and second with value 2 and so on.\n" +
            "Recorded request data can be reset with `Reset HTTP Request Contents` keyword.\n\n" +
            "== Setting response data ==\n\n" +
            "Library can be instructed to give unique responses for certain different kind of requests.\n" +
            "E.g. `Set HTTP Response Content` keyword can be used with arguments _path_, _queryString_, and _soapAction_ to set\n" +
            "unique response content for different requests. Set response data can be reset with `Reset HTTP Response Contents`\n" +
            "keyword\n\n" +
            "Examples:\n" +
            "| Set Default Response | content=Hello | statusCode=${201} | contentType=text/plain | headers=&{header_dict} |\n" +
            "| Set REST Response | path=/test | method=GET | queryParameters=&{param_dict} | content=Hello |\n" +
            "| Set SOAP Response | path=/testService | soapAction=testAction | content=${soap_xml} | |";

    public HTTPServer() {
        super("net/relaysoft/robot/http/server/keywords/*.class");
    }

    @Override
    public String getKeywordDocumentation(String keywordName) {
        if (keywordName.equals("__intro__")) {
            return INTRO;
        }
        return super.getKeywordDocumentation(keywordName);
    }
}
