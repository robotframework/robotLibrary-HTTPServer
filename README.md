# Robot Framework library for HTTP client testing.

[![Maven Central](https://img.shields.io/maven-central/v/net.relaysoft.robotframework/http-server-library.svg?style=flat-square)](http://search.maven.org/#artifactdetails%7Cnet.relaysoft.robotframework%7Chttp-server-library%7C1.0.0%7Cjar)

## Overview

Java Robot Framework for HTTP client application testing. Provides configurable HTTP server for providing mock responses and recording request content for later inspection. Library is build upon [Jetty](http://www.eclipse.org/jetty/) server.

This library can be used either with Maven Robot Framework plug-in, Robot Framework standalone JAR distribution, or as remote library with [jrobotremoteserver](https://github.com/ombre42/jrobotremoteserver).  

### Prerequisites

To execute within robot framework as standalone Java application.

* Java version 8 
* [JavalibCore](https://github.com/robotframework/JavalibCore) library.
* Jetty server 9.4.x library with its required dependencies

### Usage

Using library with Maven Robot Framework plug-in either by adding jar file directly or by dependency.

```xml
<plugin>
	<groupId>org.robotframework</groupId>
	<artifactId>robotframework-maven-plugin</artifactId>
	<configuration>
		<extraPathDirectories>
			<extraPathDirectory>path/to/http-server-library.jar</extraPathDirectory>
		</extraPathDirectories>
	</configuration>
	<dependencies>
      	<dependency>
	  		<groupId>org.robotframework</groupId>
			<artifactId>javalib-core</artifactId>
 	 	</dependency>
 	 	<dependency>
	  		<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-server</artifactId>
 	 	</dependency>
    </dependencies>
</plugin>
```

```xml
<plugin>
	<groupId>org.robotframework</groupId>
	<artifactId>robotframework-maven-plugin</artifactId>
	<dependencies>
      	<dependency>
        	<groupId>net.relaysoft.robotframework</groupId>
        	<artifactId>http-server-library</artifactId>
      	</dependency>
      	<dependency>
	  		<groupId>org.robotframework</groupId>
			<artifactId>javalib-core</artifactId>
 	 	</dependency>
    </dependencies>
</plugin>
```

Using library with Robot Framework standalone JAR distribution.

	java -cp http-server-library.jar:jetty-server-9.4.8.v20171121.jar:jetty-http-9.4.8.v20171121.jar:jetty-util-http-9.4.8.v20171121.jar:jetty-io-http-9.4.8.v20171121.jar:javax.servlet-api-3.1.0.jar:javalib-core.jar:robotframework.jar org.robotframework.RobotFramework test.robot


## Running the tests

Project unit test and robot keyword tests are executed within Maven _test_ and _verify_ phases.

Unit tests:

	mvn test
	
Integration tests (Robot keyword unit tests):

	mvn verify

## Built With

[Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting pull requests to us.

## Versioning

Uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/robotframework/robotLibrary-HTTPServer/tags). 

## Authors

* **Tapani Leskinen** - [relaysoft.net](https://relaysoft.net)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details

## Maven Dependency

```xml
<dependency>
	<groupId>net.relaysoft.robotframework</groupId>
	<artifactId>http-server-library</artifactId> 
	<version>1.0.0</version>
</dependency>
```
